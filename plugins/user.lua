return {
  -- You can also add new plugins here as well:
  -- Add plugins, the lazy syntax
  -- "andweeb/presence.nvim",
  -- {
  --   "ray-x/lsp_signature.nvim",
  --   event = "BufRead",
  --   config = function()
  --     require("lsp_signature").setup()
  --   end,
  -- },
  { 
    "jamestthompson3/nvim-remote-containers",
    enable = false,
  },
  {
    name = tabby, 
    lazy = false,
    dir = "~/tabby/clients/vim", 
    enable = false,
  },
  {
    'wuelnerdotexe/vim-astro',
    enable = true,
  }
}
