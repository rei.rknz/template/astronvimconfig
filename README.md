# AstroNvim Reirknz configuration

This is my AstroNvim config.

## My plugin list

- Tabby
  - https://github.com/TabbyML/tabby/tree/main/clients/vim

```shell
git clone https://github.com/TabbyML/tabby.git ~/tabby
```

## 🛠️ Installation

#### Make a backup of your current nvim and shared folder

- Windows

```powershell
Rename-Item -Path $env:LOCALAPPDATA\nvim -NewName $env:LOCALAPPDATA\nvim.bak
Rename-Item -Path $env:LOCALAPPDATA\nvim-data -NewName $env:LOCALAPPDATA\nvim-data.bak
```

- Linux

```shell
mv ~/.config/nvim ~/.config/nvim.bak
mv ~/.local/share/nvim ~/.local/share/nvim.bak
```

#### Clone AstroNvim

- Windows

```
git clone --depth 1 https://github.com/AstroNvim/AstroNvim $env:LOCALAPPDATA\nvim
```

- Linux

```shell
git clone https://github.com/AstroNvim/AstroNvim ~/.config/nvim
```

#### Clone the repository

- Windows

```shell
git clone git@gitlab.com:rei.rknz/template/astronvimconfig.git $env:LOCALAPPDATA\nvim\lua\user
```

- Linux

```shell
git clone git@gitlab.com:rei.rknz/template/astronvimconfig.git ~/.config/nvim/lua/user
```

#### Start Neovim

```shell
nvim
```
